from django.db import models
from django.utils import timezone
class Product(models.Model):
    name = models.CharField(max_length=100, default="")
    #text = models.CharField(max_length=100, default="")
    image = models.ImageField(upload_to="images/")
    price = models.PositiveIntegerField(default=0)
    def __str__(self):
        return str(self.id)

class Payment(models.Model):
	cost = models.PositiveIntegerField(default=0)
	date = models.DateTimeField()
	payment_type = models.PositiveIntegerField(default=0)
	user = models.ForeignKey('auth.User')
    
