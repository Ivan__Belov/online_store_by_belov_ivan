from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from paypal.standard.forms import PayPalPaymentsForm
from .models import Product, Payment
from django.views.decorators import cache 
from cart.cart import Cart
import time
import datetime
from django.core.cache import cache

def home_page(request): 
    return render(request, 'online_shop/home_page.html', {'items': cache.get_or_set('items', Product.objects.filter().order_by('name'), 10)})

def product(request, product_id):
    product = Product.objects.get(pk=product_id)
    return render(request, 'online_shop/product.html', {'product': product})

def basket(request):
    cart = Cart(request)
    return render(request, 'online_shop/basket.html', {'cart' : cart})
   


def add_to_cart(request):
    product_id = request.POST.get('product_id')[0]   
    product = Product.objects.get(pk=product_id)
    cart = Cart(request)
    quantity = 1
    cart.add(product, product.price, quantity)
    return HttpResponse()

def remove_from_cart(request):
    product_id = request.POST.get('product_id')[0]
    print(product_id)
    product = Product.objects.get(pk=product_id)
    cart = Cart(request)
    cart.remove(product)
    return HttpResponse()

def account_logout(request):
    """
    Logout and redirect to the main page.
    """
    logout(request)
    return redirect('/')

def account_profile(request):
    """
    Show user greetings. ONly for logged in users.
    """
    return redirect('/')

def home(request):
    """
    Home page with auth links.
    """
    if request.user.is_authenticated():
        return HttpResponse("{0} <a href='/accounts/logout'>exit</a>".format(request.user))
    else:
        return HttpResponse("<a href='/login/vk-oauth2/'>login with VK</a>")

@csrf_exempt
def paypal_success(request):
    cart = Cart(request)
    payment = Payment(payment_type=1, date=datetime.date.today(), cost=cart.summary(), user=request.user)
    payment.save()
    cart.clear()
    return HttpResponse("Money is mine. Thanks.")    

@login_required
def paypal_pay(request):
    cart = Cart(request)
    items = list()
    for elem in cart:
        items.append(elem.product.name)
    paypal_dict = {
        "business": "vanja9.10.96-facilitator@gmail.com",
        "amount": cart.summary(),
        "currency_code": "RUB",
        "item_name": items,
        "invoice": "INV-" + str(int(time.time())),
        "notify_url": reverse('paypal-ipn'),
        "return_url": "http://localhost:8000/payment/success/",
        "cancel_return": "http://localhost:8000/payment/cart/",
        "custom": str(request.user.id)
    }
# Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form, "paypal_dict": paypal_dict}
    return render(request, "online_shop/payment.html", context)
