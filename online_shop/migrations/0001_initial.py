# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(default='', max_length=100)),
                ('image', models.ImageField(upload_to='images/')),
                ('price', models.PositiveIntegerField(default=0)),
            ],
        ),
    ]
