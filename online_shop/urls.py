from django.conf.urls import include, url
from django.contrib import admin
from . import views
import social.apps.django_app.urls
import paypal.standard.ipn.urls

urlpatterns = [
    url(r'^$', views.home_page, name='home_page'),
    url(r'^payment/cart/$', views.paypal_pay, name='cart'),
    url(r'^payment/success/$', views.paypal_success, name='success'),
    url(r'^paypal/', include(paypal.standard.ipn.urls)),
    url(r'^accounts/logout/$', views.account_logout, name='logout'),
    url(r'^accounts/login/$', views.home, name='login'),
    url(r'^accounts/profile/$', views.account_profile, name='profile'),
    url(r'^add-to-cart/$', views.add_to_cart, name='add'),
    url(r'^remove-from-cart/$', views.remove_from_cart, name='remove'),
    url('', include(social.apps.django_app.urls, namespace='social')),
    url(r'^product/(?P<product_id>\d+)/$', views.product, name='product'),
    url(r'^basket/$', views.basket, name='basket'),
]
